#ifndef BUTTON_H_
#define BUTTON_H_

#include "stm32f1xx_hal.h"
#include  <cinttypes>

class Button {
private:
	GPIO_TypeDef *port;	///< port related to managed input
	uint16_t pin;		///< pin related to managed input
	bool filteredState;	///< debounced output

	uint32_t timePreviousAction;		///< timestamp for previous press() method call
	const uint32_t TIME_DEBOUNCE_MIN;	///< time since last press() method call, for which edge events are ignored

	void (*callbackPress)(void);	///< function called when button press is registered
	void (*callbackRelease)(void);	///< function called when button release is registered

public:
	Button(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint32_t debounceTime=40);
	void process();
	bool isPressed();
	void registerPressCallback(void(*callback)());
	void registerReleaseCallback(void(*callback)());
	auto getPressCallback() -> void(*)();
	auto getReleaseCallback() -> void(*)();
	virtual ~Button();
};

#endif /* BUTTON_H_ */
