#ifndef SRC_LEDSTATE_H_
#define SRC_LEDSTATE_H_

#include <cinttypes>

/**
 * Enum describing three states of LED operation used in the code.
 * Does allow incrementation and decrementation.
 */
enum class LedState : uint8_t
{
	ON,
	OFF,
	BLINKING
};

/*
 * Pre- and post- incrementation and decrementation of enum
 * dummy int means that this is postfix "i++" version
 * Is that what's called an overkill?
 */
LedState& operator++( LedState &c );
LedState operator++( LedState &c, int );

LedState& operator--( LedState &c );
LedState operator--( LedState &c, int );

#endif /* SRC_LEDSTATE_H_ */
