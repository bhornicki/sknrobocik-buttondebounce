#include "Button.h"

/**
 * @brief initialize parameters for debouncing of interrupt enabled pin
 * @details Input pin should be in interrupt mode "GPIO_MODE_IT_RISING_FALLING".
 * <br> Pin should bave enabled internal pullup, with button getting it to ground when pressed.
 * @param GPIOx	pointer to port typedef of input which will be debounced
 * @param GPIO_Pin pin number of input which will be debounced
 * @param debounceTime time in ms describing how long will be the pin edge interrupts ignored
 * @todo  change GPIO_TypeDef nad GPIO_Pin to const
 * <br> assert_param test for pin interrupts being anabled
 */
Button::Button(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint32_t debounceTime)
	:TIME_DEBOUNCE_MIN(debounceTime)
{
	port=(GPIOx);
	pin=(GPIO_Pin);
	filteredState=0;

	callbackPress=nullptr;
	callbackRelease=nullptr;
}

Button::~Button()
{

}

/**
 * @brief Process the debouncing of the pin
 * @details Call this method from HAL_GPIO_EXTI_Callback after filtering interrupt input pin
 * <br> If rising or falling edge interrupt for this class object is registered,
 * it will be called from here
 * <br> The main idea is to ignore pin changes if previous one was registered within the specified
 * time.
 */
void Button::process()
{
	//true when button is pressed
	bool realState = HAL_GPIO_ReadPin(port, pin) == GPIO_PinState::GPIO_PIN_RESET;

	//don't process when interrupt was fired but the read pin value matches current dabounced value
	if(realState==filteredState)
		return;

	//update timestamp of previous cal of this method from interrupt
	uint32_t time = HAL_GetTick();
	uint32_t diff = (time-timePreviousAction);
	timePreviousAction = time;
	//don't accept events if timeDebounceMin heven't passed yet - the mystery of debouncing
	if(diff < TIME_DEBOUNCE_MIN)
		return;

	//filtering done, time to update debounced state and call callbacks
	filteredState = !filteredState;
	if (realState)
	{
		if(callbackPress)
			callbackPress();
	}
	else
	{
		if(callbackRelease)
			callbackRelease();
	}



}

/**
 *
 * @return pin state after debouncing process
 */
bool Button::isPressed()
{
	return filteredState;
}

/**
 * @brief Register callback called after registering falling edge - button press event
 * @details using this method again will overwrite previously registered callback.
 * Passing nullptr will disable this callback.
 * @param pointer to callback function without arguments
 */
void Button::registerPressCallback(void(*callback)())
{
	this->callbackPress=callback;
}

/**
 *
 * @brief register callback called after registering rising edge - button release event
 * @details using this method again will overwrite previously registered callback.
 * Passing nullptr will disable this callback.
 * @param pointer to callback function without arguments
 */
void Button::registerReleaseCallback(void(*callback)())
{
	this->callbackRelease=callback;
}

/**
 * Get current pointer to callback function for falling edge - button press event
 * @return pointer to callback function without arguments
 */
auto Button::getPressCallback() -> void(*)()
{
	/*
	 * It's getting better and better :D
	 * Now You can pass lambda to registerPressCallback
	 * and reuse it in ReleaseCallback xDDD
	 * button->registerReleaseCallback(button->getPressCallback());
	 */
	return callbackPress;
}

/**
 * Get current pointer to callback function for rising edge - button release event
 * @return pointer to callback function without arguments
 */
auto Button::getReleaseCallback() -> void(*)()
{
	return callbackRelease;
}
