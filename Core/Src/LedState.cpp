#include "LedState.h"

const LedState LAST_STATE = LedState::BLINKING;	///< the last possible state (highest value) of enum

LedState& operator++(LedState &c)
{
	if (c == LAST_STATE)
		c = static_cast<LedState>(0);
	else
		c = static_cast<LedState>(static_cast<uint8_t>(c) + 1);
	return c;
}

LedState operator++(LedState &c, int)
{
	LedState result = c;
	++c;
	return result;
}

LedState& operator--(LedState &c)
{

	if (c == static_cast<LedState>(0))
		c = LAST_STATE;
	else
		c = static_cast<LedState>(static_cast<uint8_t>(c) - 1);
	return c;
}

LedState operator--(LedState &c, int)
{
	LedState result = c;
	--c;
	return result;
}
