# zad6 Obsługa programowa drgań styków

## Założenia
- Dioda "LED" pod PC13 (zintegrowana na bluepillu), 
- przycisk "BUTTON" PB12 w trybie przerwań na obu zboczach. 
- Przycisk to wylutowany microswitch, zakładam większy wpływ drgań styków w związku ze zużyciem.
- Kod w postaci klasy CPP, z callbackami po wciśnięciu/zwolnieniu przycisku.

Datasheet podaje minimalne napięcie stanu wysokiego jako 1.833, maksymalne stanu niskiego to z kolei 1.164. 

## Opis działania
Do inicjalizatora obiektu klasy przekazujemy port i pin, którego zbocza będziemy obsługiwać. Podajemy też czas na stabilizację sygnału.

Wybrany pin powinien mieć włączone przerwania na zboczu narastającym i opadającym.
W funkcji przerwania zbocza należy wywołać metodę process dla obiektu obsługującego daną klasę.

Status pinu możemy odczytać poprzez funkcję isPressed, bądź rejestrując callbacki dla zboczy narastającego i opadającego, które zostaną wywołane przy zmianie "odfiltrowanego" stanu.

Zmiana stanu następuje gdy:

1. Aktualny stan na pinie różni się od "przefiltrowanego" stanu podawanego przez klasę
2. Poprzednie przerwanie spełniające warunek 1 nastąpiło w czasie większym niż ustalony na ustabilizowanie stanu

Gdy te warunki zostaną spełnione, stan wskazywany przez metodę isPressed się zmieni i zostanie wywołany odpowiedni callback.

## Etapy
0. Miganie diodą w celu sprawdzenia czy podrobiony JLink nie gryzie się z podrobionym bluepillem
1. LED podąża za stanem BUTTON. Pod LED podpięty oscyloskop w celu analizy wykrytych drgań i zależności czasowych
2. LED ustawiany w callbacku klasy zajmującej się drganiem styków
3. Wprowadzenie trzech stanów diody - wyłączona, włączona, migająca

## Pomiary oscyloskopem
Chwilowo nie posiadam żadnego wolnego pendrive sformatowanego w fat32 ani przygotowanego oprogramowania do przechwytywania danych poprzez ethernet lub USB, więc niestety pozostają zdjęcia.

Żółty kanał oscyloskopu przedstawia sygnał wejściowy na pinie "BUTTON", niebieski tosygnał wyjściowy na pinie "LED".

Datasheet podaje miimalne napięcie stanu wysokiego jako 1.833, dlatego ta wartość została wybrana jako poziom triggera dla kanału wejściowego.

W przypadku *img1* i *img2* wyjście podąża za wejściem, to znaczy w przerwaniu pinu "BUTTON" ustawiamy stan "LED" na bieżący stan pinu "BUTTON".

*Img1* przedstawia pierwszy microswitch, który został odrzucony - ilość szumów jasno sugeruje, że mikroprzycisk należy wymienić na nowy - *img2* przedstawia kolejną sztukę.

W przypadku *img3* i *ing4* wyjście zmieniane jest na stan przeciwny w momencie gdy klasa obsługująca drgania styków wykryje wciśnięcie/puszczenie przycisku.

*Img3* przedstawia niewykryte zbocze narastające. Jest to skutek za krótkiego wciśnięcia przycisku względem zaprogramowanego czasu oczekiwania na ustabilizowanie sygnału.

Widzimy też, że zbocze narastające zostało zgłoszone przy pierwszym zboczu narastającym po dłuższym okresie stabilnego sygnału. 
Zgłoszenie zbocza narastającego w momencie przejścia sygnału w ciągły stan wysoki jest możliwe do zaprojektowania, ale wymaga sprawdzenia stanu pinu chwilę po przerwaniu zbocza narastającego, na przykład dodając kod w przerwaniu timera lub funkcji systick.
Funkcjonalność ta nie jest dla nas kluczowa, dlatego zaimplementowalem tylko podstawową wersję.

*Img4* pokazuje zbocze opadające w innej rozdziałce czasowej. Widzimy, że problem występuje też w skali mniejszej, niż widocznej na pozostałych zdjęciach.
Najgorzej przedstawiają się pierwsze 1.5ms zminay stanu na niski.

## Czas trwania naciśnięcia
Szybkie naciśnięcie przez człowieka trwa minimum 50ms, średnio 100ms.

Granicę 50ms da się przekroczyć, przez "oszukane naciśnięcie" (przedmiot poruszający się szybko w bok i dół jednocześnie, podczas ruchu następuje wciśnięcie przycisku, a następnie przedmiot się ześlizguje i zwalnia przycisk).
W tym przypadku klasa obsługująca przycisk nie będzie się zachowywać poprawnie.

Pokazuje to słaby punkt każdego programowego systemu filtrowania drgań styków bazującego na oczekiwaniu - przy odpowiednio szybkim klikaniu całość przestaje być stabilna. 
Można przesunąć tą granicę zmniejszając czas oczekiwania, ale należy się wtedy liczyć z drganiem styków, które nie zostanie odfiltrowane.

## Alternatywna implementacja sprzętowa
Dwa rezystory, kondensator i przerzutnik Schmitta. 
Z racji że mam tylko zaproponować rozwiązanie, to podam źródło całkiem dobrze opisujące zasadę działania:
https://mansfield-devine.com/speculatrix/2018/04/debouncing-fun-with-schmitt-triggers-and-capacitors/
